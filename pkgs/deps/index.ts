
export type OnCircularResolver= ( previewBreaker:()=>void, currentBreak:()=>void, depends:DependencyList )=>void;

export type DependencyList = Dependency[]
export function dependencyList( comparator:DependencyComparator, ...array:DependencyList ):DependencyList{
    comparator = comparator || DEFAULT_COMPARATOR;
    array.includes = ( dep:Dependency, from:number )=>{
        for (let i = from||0; i < array.length; i++) {
            let value = array[i];
            if( comparator( dep, value ) ) return true;
        }
        return false;
    }
    array.indexOf =  ( dep:Dependency, from:number )=>{
        for (let i = from||0; i <array.length; i++) {
            let value = array[i];
            if( comparator( dep, value ) ) return i;
        }
        return -1;
    }
    return array;
}

export type DependencyComparator = ( self:Dependency, other:Dependency )=>boolean



export type ResolverOpts = {
    onCircularResolver?:OnCircularResolver,
    comparator?:DependencyComparator
}

export const DEFAULT_COMPARATOR:DependencyComparator = (self, other) => {
    if( self instanceof Dependency && other instanceof Dependency && self === other ) return true;
    if( self instanceof Dependency && other instanceof Dependency && self.id === other.id ) return  true;
    if( self instanceof Dependency && typeof other === "string" && self.id === other ) return true;
    if( typeof self === "string" && other instanceof Dependency && self === other.id ) return true;
    return typeof self === "string" && typeof other === "string" && self === other;
}


export const DEFAULT_CIRCULAR_RESOLVER:OnCircularResolver = ( ( previewBreaker, currentBreak, depends) => {
    currentBreak();
})

export class DependencyResolver {

    private readonly _collection:DependencyList;
    private readonly _pendents:DependencyList;
    private readonly _list:DependencyList;
    private readonly _onCircularResolver:OnCircularResolver
    private readonly _comparator:DependencyComparator;


    constructor( opts?:ResolverOpts ) {
        if( !opts ) opts = {};
        if( typeof opts.onCircularResolver !== "function" ) opts.onCircularResolver = DEFAULT_CIRCULAR_RESOLVER;
        if( typeof opts.comparator !== "function" ) opts.comparator = DEFAULT_COMPARATOR;

        this._onCircularResolver = opts.onCircularResolver;
        this._comparator = opts.comparator;
        this._collection = dependencyList( this._comparator );
        this._pendents = dependencyList( this._comparator );
        this._list = dependencyList( this._comparator );
    }

    get list(): DependencyList {
        return this._list;

    } collect( dep:Dependency ) {
        if( !dep ) return null;
        this._collection.push( dep );
    } create( id:string, value ):Dependency{
        const  deps = new Dependency( id, this._comparator, value );
        this._collection.push( deps );
        return deps;
    } ignores() {
        this._collection.forEach( (next, nextIndex) => {
            this._collection.filter( (preview, previewIndex) => {

                return previewIndex < nextIndex
                    && next.recursiveDependency().includes( preview )
                    && preview.recursiveDependency().includes( next )
            }).forEach( preview => {
                let isCalled = false;
                let depends = next.recursiveDependency() ;
                // depends.push( next );

                this._onCircularResolver( ()=>{
                    if( isCalled ) throw new Error( "Break is executed" );
                    isCalled = true;
                    preview.ignore( next );
                }, () => {
                    if( isCalled ) throw new Error( "Break is executed" );
                    isCalled = true;
                    next.ignore( preview )
                }, depends );
                isCalled = isCalled
                    || preview.ignored.includes( next )
                    || next.ignored.includes( preview )
                ;
                if( next.recursiveDependency().includes( preview ) && preview.recursiveDependency() .includes( next ) )
                    throw new Error( "Circular dependency not solved!" );
            });
        });
        this._collection.forEach(next =>{
            if( next.use().length ) this._pendents.push( next )
            else this._list.push( next );
        });
    } order() {
        let total = this._collection.length;
        let maxLoop = total * total * total;

        let loop = 0;
        let next:Dependency;
        while ( next = this._pendents.shift() ){
            if( loop === maxLoop ) break;
            let recursive = next.recursiveDependency();
            let notInclude =  recursive.find( value => !this._list.includes( value ) );


            loop++;
            if( notInclude ){
                this._pendents.push( next );
                continue
            }
            this._list.push( next );
        }
    } clean( verbose?:boolean ){
        this._list.splice( 0, this._list.length );
        this._pendents.splice( 0, this._pendents.length );
        if( verbose ){
            this._collection.forEach( dep => dep.clean() );
            this._collection.splice( 0, this._pendents.length );
        }
    }
}

export class Dependency {
    private readonly _id: string
    private readonly _depends: DependencyList ;
    private readonly _ignore: DependencyList;
    private _value:any;
    private readonly _comparator:DependencyComparator;

    constructor( id: string, comparator?:DependencyComparator, value?:any ) {
        this._id = id;
        this._comparator = comparator || DEFAULT_COMPARATOR;
        this._depends = dependencyList( this._comparator );
        this._ignore = dependencyList( this._comparator );
        this._value = value;
    }

    get ignored() {
        return this._ignore;
    } get depends(){
        return this._depends;
    } get id(){
        return this._id;
    } value<T>( val?:T ):T {
        if( val !== undefined ) this._value = val;
        return this._value;
    } use(){
        return this._depends.filter( value => !this._ignore.includes( value ) )
    } ignore( mode:Dependency ){
        if( this.recursiveDependency().includes( mode ) && !this._ignore.includes( mode ) ) {
            this._ignore.push( mode );
        }
    } clean() {
        this._ignore.splice( 0, this._ignore.length );
    } equals( dep:Dependency ):boolean{
        return this._comparator( this, dep );
    }

    recursiveDependency(cash?:DependencyList, from?:Dependency ):DependencyList{
        if( !cash ) cash = dependencyList( this._comparator );
        if( !cash.includes( this ) ) cash.push( this );
        const recBefore = dependencyList( this._comparator );
        this.use().forEach( next => {
            if( !cash.includes( next ) ) recBefore.push( ... ( next.recursiveDependency( cash, this ).filter( recursiveDep =>
                !this._ignore.includes( recursiveDep )
            )) );
            if( next !== from ) recBefore.push( next );
        });
        return  recBefore;
    } after( ...modes:DependencyList ){
        modes.forEach( mode => {
            if( !mode ) return null;
            mode.before( this );
        });
        return this;
    } before ( ... modes:DependencyList ){
        modes.forEach( mode => {
            if( !mode ) return null;
            if( mode.equals( this ) ) return this;
            mode._depends.push( this );
        })
        return  this;
    } toString(){
        return JSON.stringify( { name: this._id, afters: this._depends.map(value => value._id )})
    }
}
