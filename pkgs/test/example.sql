/***!
@id( naidsksjs )
*/
alter table my_table_name;

/**!
  @id( 73839830-9393 )
  @upgrade
  @one
  @after( myTableId otherId )
*/
update pg_tables set schemaname = 'simple' where true;

/**!
  @id(myFuncId)
  @upgrade
  @after( myTableId, sdsds )
!*/
drop function if exists fn( jsonb );
create or replace function fn ()
returns boolean
language plpgsql as $$
  declare
  begin

  end;
$$;

/**!
  @id(myTableId)
  @upgrade
  @one
!*/
drop table if exists my_table_name;
create table my_table_name(
  id serial,
  text text
);