import {Path} from "./path";
import fs from "fs";
import fsPath from "path";


export type ScanListener = ( p:Path )=>void;
export type ScanOpts = {
    recursive?:boolean
}

const _recursiveScanDir = function ( dir:string, opts:ScanOpts, filters:ScanFilter[], listeners:ScanListener[], base?:string ):Path[]|null {

    if (!fs.existsSync( dir )){
        return null;
    }

    let sub:string;
    dir =  fsPath.resolve( dir+ Path.osSep ) + Path.osSep;
    if( !base ) base = dir;

    const accepts:Path[] = [];
    const files = fs.readdirSync( dir );
    for( let i=0; i<files.length; i++){
        const file:string = files[ i ];
        const path:string = fsPath.join( dir, files[i] );
        const stat = fs.lstatSync( path );
        sub = path.substr( base.length, path.length - base.length - file.length );
        const relative:string = `${sub}${ file }`;

        const _path = new Path({ base, sub, file, dir, relative, path } );
        if ( opts.recursive && stat.isDirectory() ){
            const _paths = _recursiveScanDir( path, opts, filters, listeners, base );
            if( _paths ) accepts.push( ... _paths  );
        } else {
            const push = function () {
                listeners.forEach( value => value( _path ) );
                accepts.push( _path );
            }
            if( filters.find( next => next.test( _path ) ) ) push();
        }
    }
    return accepts;
}

export type FilterType =  "extension"|"regx"|"function";
export type ScanFilterOpts = { not?:boolean }
export class ScanFilter {
    private type:FilterType
    private value: RegExp|(( path:Path )=>boolean )
    private readonly ands:ScanFilter[]= [];
    private _opts:ScanFilterOpts;

    constructor( type:FilterType, value: RegExp|(( path:Path )=>boolean ), opts?:ScanFilterOpts ) {
        if( !opts ) opts = {};
        this.value = value;
        this._opts = opts;
    } not( ...filters:ScanFilter[] ):ScanFilter{
        filters.forEach( next => {
            next._opts.not = true;
            this.and( next );
        })
        return  this;
    } and( ...filters:ScanFilter[] ):ScanFilter{
        filters.forEach( next => {
            this.ands.push( next );
        })
        return this;
    } test( path:Path ){
        let filters:ScanFilter[] = [ ... this.ands ];
        let thisTest = ( this.value instanceof RegExp )? this.value.test( path.path )
            : this.value( path );
        if( this._opts.not ) thisTest = !thisTest;
        return thisTest && !filters.find( value => !value.test( path ) );
    }
}

export function extension( extname:string, opts?:ScanFilterOpts  ):ScanFilter{
    return new ScanFilter( "extension", new RegExp(`.*.(${ extname })`), opts );
}

export function filter( callback:( path:Path )=>boolean, opts?:ScanFilterOpts ) :ScanFilter{
    return new ScanFilter( "function", callback, opts );
}

export function regExp(  regx:RegExp, opts?:ScanFilterOpts ) :ScanFilter{
    return new ScanFilter( "regx", regx, opts );
}



export function scanDir (dir:string, opts:ScanOpts|ScanListener|ScanFilter|RegExp, ...maths:(ScanListener|ScanFilter|RegExp)[] ) {
    let  _opts:ScanOpts;
    let listens:ScanListener[] = [];
    let filters:(ScanFilter)[]= [];

    if( typeof opts === "function"|| opts instanceof RegExp || opts instanceof ScanFilter ){
        maths.unshift( opts );
    } else if( opts && typeof opts === "object" ) _opts = opts;

    maths.forEach( next => {
        if( next instanceof RegExp ) filters.push( regExp( next ) );
        if( next instanceof ScanFilter ) filters.push( next );
        if( typeof next === "function" ) listens.push( next );
    })

    if( !_opts ) _opts = {};

    return _recursiveScanDir( dir, _opts, filters, listens );
}

