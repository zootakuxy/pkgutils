import fsPath from "path";

export interface PathOpts  {
    base:string; sub:string; file:string; dir:string; relative:string; path:string
}
type PathKeys = { [P in keyof PathOpts]?:string }

export function createUrl( protocol, path:string, ...args):URL{
    let url = [ protocol, path, ...(args.filter( value => !!value )) ];
    return new URL( url.join( ":" ) );
}
export function createUrlFile( path:string, line?:number, column?:number):URL{
    return  createUrl( "file", path, line, column  );
}
export class Path implements PathKeys  {
    base:string; sub:string; file:string; dir:string; relative:string; path:string
    static get regexpDelimiters() { return /[\\$]|[\/$]/ }
    static get osSep() { return fsPath.sep };

    constructor( path:PathOpts ) {
        Object.keys( path ).forEach( key =>{
            this[ key ] = path[ key ];
        });
    }

    get url() { return createUrlFile( this.path )}
}