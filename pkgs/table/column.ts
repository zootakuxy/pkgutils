export type ColumnOpts = {
    title:string,
    len:number,
    index:number,
}

export class Column implements ColumnOpts {
    title:string
    len:number
    index:number

    constructor( opts?:ColumnOpts ) {
        Object.assign( this, opts );
    }
}

export function columnOf( opts?:ColumnOpts ):Column{
    if( opts instanceof Column ) return opts;
    else return new Column( opts );
}