import {Border, BorderSpecs, Cell, HorizontalBorder, VerticalBorder} from "./cell";
import {Line} from "./line";

export type BorderStyle = { [ p in keyof typeof DEFAULT_BORD_STYLE ]:string };

export const DEFAULT_BORD_STYLE = {
    top: `─`,
    left: `│`,
    right: `│`,
    bottom: `─`,
    "top-left": `┌`,
    "top-right": `┐`,
    "bottom-left": `└`,
    "bottom-right": `┘`,

    "top-join": `┬`,
    "bottom-join": `┴`,
    "left-join": `├`,
    "right-join": `┤`,

    join: `┼`,
    "horizontal-join": `─`,
    "vertical-join": `│`,
} as const;

export class StyleEngine {
    _map:  ({keys:string[], value:string })[] = [];
    style:BorderStyle;
    constructor( bs:BorderStyle ) {
        this.style = bs;
        Object.keys( bs ).forEach( ( key, index) => {
            let keys = key.split( "-" ).sort();
            let value = bs[ key ];
            this._map.push( { keys, value } );
        })
    }

    find( ...keys:((keyof Border)|"join"|"horizontal")[]):string{
        let found = this._map.find( value => value.keys.join("-") === keys.sort().join("-" ) )?.value;
        return found? found:"";
    }
}

export type LateralBorders = ({[p in keyof VerticalBorder]:string}) & {
    chars:number
};
export function lateralBorders( style:StyleEngine,  bs:BorderSpecs, horizontal?:(keyof HorizontalBorder) ):LateralBorders{
    const laterals:(keyof VerticalBorder )[] = [ "left", "right" ];
    let _borders:LateralBorders = {
        left:"",
        right:"",
        get chars(){ return this.left.length + this.right.length },
    };

    laterals.forEach( lateral => {
        let margins:(keyof Border)[] = [ lateral ];
        if( horizontal ) margins.push( horizontal );
        if( lateral ===  "left" && bs.is( ...margins ) ) _borders.left = style.find( ... margins );
        if(  lateral === "right" && bs.is( ...margins ) ) _borders.right = style.find( ... margins );
    });
    return  _borders;
}

export type BorderEngine = {
    topLine:string,
    bottomLine:string,
    topCellLine:string,
    bottomCellLine: string,
    lines:string[];
}

export function cellLengthWithOutBorder(cell:Cell, initialLength:number ):number{
    return  initialLength;
}

function startLines(){
    return {
        topTable:{ left:[], right:[] },
        topLine:{ left:[], right:[] },
        topCell:{ left:[], right:[] },
        content:{ left:[], right:[] },
        bottomCell:{ left:[], right:[] },
        bottomLine:{ left:[], right:[] },
        bottomTable:{ left:[], right:[] }
    }
}

export type StatusOpt = {
    cellBorder:BorderSpecs,
    lineBorder:BorderSpecs,
    tableBorder:BorderSpecs,
    line:Line
    cell:Cell,
    splitLine:boolean,
    preview:Line,
    next:Line,
}
export function borderEngine( status:StatusOpt){
    let _lines = startLines();
    let hasPreview = !!status.preview;
    let hasNext = !!status.next;
    let isFistLine = !status.preview;
    let isLastLine = !status.next;
    let isBetweenLine = !!status.next && !!status.preview;
    let joinBottom = !status.splitLine && ( isBetweenLine || ( !isLastLine && hasPreview ) );

    //Solve Content
    if( status.cellBorder.left ) _lines.content.left.push( DEFAULT_BORD_STYLE[ "left" ] );
    if( status.cellBorder.right ) _lines.content.right.push( DEFAULT_BORD_STYLE[ "right" ]);

    if( status.lineBorder.left && status.cell.isFirst ) _lines.content.left.push( DEFAULT_BORD_STYLE[ "left" ]);
    if( status.lineBorder.right && status.cell.isLast ) _lines.content.right.push( DEFAULT_BORD_STYLE [ "right" ]);

    if( status.tableBorder.left && status.cell.isFirst ) _lines.content.left.push( DEFAULT_BORD_STYLE[ "left" ]);
    if( status.tableBorder.right && status.cell.isLast ) _lines.content.right.push( DEFAULT_BORD_STYLE [ "right" ]);

    //Solve TopCell
    if( status.cellBorder.topLeft ) _lines.topCell.left.push( DEFAULT_BORD_STYLE[ "top-left" ] );
    if( status.cellBorder.topRight ) _lines.topCell.right.push(  DEFAULT_BORD_STYLE[ "top-right" ] );

    if( status.lineBorder.topLeft && status.splitLine ) _lines.topCell.left.push( DEFAULT_BORD_STYLE[ "left" ] );
    if( status.lineBorder.topRight && status.splitLine ) _lines.topCell.right.push( DEFAULT_BORD_STYLE[ "right"])

}



function example3LineSplit(){
return`
┌─────────────┐   
│┌───┬───┬───┐│   
││┌─┐│┌─┐│┌─┐││   
│││C│││C│││C│││   
││└─┘│└─┘│└─┘││   
│└───┴───┴───┘│   
│┌───┬───┬───┐│   
││┌─┐│┌─┐│┌─┐││   
│││C│││C│││C│││   
││└─┘│└─┘│└─┘││   
│└───┴───┴───┘│   
│┌───┬───┬───┐│   
││┌─┐│┌─┐│┌─┐││   
│││C│││C│││C│││ 
││└─┘│└─┘│└─┘││ 
│└───┴───┴───┘│ 
└─────────────┘
`.split("\n" ).filter( value => value.trim().length )
    .map( (value, index, array) => ({
        key:(
            (false) ?""
            :(index === 0)?"tt1"
            :(index === 1)?"tl2"
            :(index === 2)?"tc3"
            :(index === 3)?"ct3"
            :(index === 4)?"bc3"
            :(index === 5)?"bl2"
            :(index+1 === array.length)?"bt1"
            :null
        ),
        value: value.trim()
    })).filter( (value) => value.key )
    .map( value => {
        let s = {
            orientation:value.key[0]==="t"? "top":"bottom",
            left: value.value.substr(0, Number(value.key[2])),
            right: value.value.substr( value.value.length-Number(value.key[2]), Number(value.key[2]))

        };
    })
}


/*

┌─────────────┐   ┌─────────────┐  ┌─────────────┐
│┌───┬───┬───┐│   │┌───┬───┬───┐│  │┌───┬───┬───┐│
││┌─┐│┌─┐│┌─┐││   ││┌─┐│┌─┐│┌─┐││  ││┌─┐│┌─┐│┌─┐││
│││C│││C│││C│││   │││C│││C│││C│││  │││C│││C│││C│││
││└─┘│└─┘│└─┘││   ││└─┘│└─┘│└─┘││  ││└─┘│└─┘│└─┘││
│└───┴───┴───┘│   │└───┴───┴───┘│  │└───┴───┴───┘│
│┌───┬───┬───┐│   │┌───┬───┬───┐│  └─────────────┘
││┌─┐│┌─┐│┌─┐││   ││┌─┐│┌─┐│┌─┐││
│││C│││C│││C│││   │││C│││C│││C│││
││└─┘│└─┘│└─┘││   ││└─┘│└─┘│└─┘││
│└───┴───┴───┘│   │└───┴───┴───┘│
│┌───┬───┬───┐│   └─────────────┘
││┌─┐│┌─┐│┌─┐││
│││C│││C│││C│││
││└─┘│└─┘│└─┘││
│└───┴───┴───┘│
└─────────────┘
┌─────────────┐   ┌─────────────┐  ┌─────────────┐
│┌───┬───┬───┐│   │┌───┬───┬───┐│  │┌───┬───┬───┐│
││┌─┐│┌─┐│┌─┐││   ││┌─┐│┌─┐│┌─┐││  ││┌─┐│┌─┐│┌─┐││
│││C│││C│││C│││   │││C│││C│││C│││  │││C│││C│││C│││
││└─┘│└─┘│└─┘││   ││└─┘│└─┘│└─┘││  ││└─┘│└─┘│└─┘││
│├───┼───┼───┤│   │├───┼───┼───┤│  │└───┴───┴───┘│
││┌─┐│┌─┐│┌─┐││   ││┌─┐│┌─┐│┌─┐││  └─────────────┘
│││C│││C│││C│││   │││C│││C│││C│││
││└─┘│└─┘│└─┘││   ││└─┘│└─┘│└─┘││
│├───┼───┼───┤│   │└───┴───┴───┘│
││┌─┐│┌─┐│┌─┐││   └─────────────┘
│││C│││C│││C│││
││└─┘│└─┘│└─┘││
│└───┴───┴───┘│
└─────────────┘

























 */

