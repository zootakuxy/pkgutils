import {alignLine } from "@pkgutils/justify";

import { BorderStyle, DEFAULT_BORD_STYLE, lateralBorders, StyleEngine} from "./border";
import {Line, lineOf, LineOpts} from "./line";
import {Column} from "./column";
import {
    Border,
    borderOf,
    BorderOpts,
    BorderSpecs,
    Cell,
    CellItem,
    CellStyle, HorizontalBorder,
    Padding,
    spaceOf,
    Specs
} from "./cell";



export type TableOpts = {
    cellsPadding?:Padding,
    cellsBorders?:BorderOpts,

    rowsPadding?:Padding,
    rowsBorders?:BorderOpts,

    columnsPadding?:Padding
    columnsBorders?:BorderOpts

    borderStyle?:BorderStyle,
    splitLines?:boolean
}

export type Table = (( opts?:LineOpts, ...cells:CellItem[] )=>void) & TableCell;

export function createTable( opts?:TableOpts ):Table{
    let table = new TableCell(opts);

    return new Proxy(Object.assign(( opts?: LineOpts, ...cells: CellItem[]) => {
        if( !opts && !cells.length ){
            table.calculate();
            return table.show();
        } return table.push(opts, ...cells );
    }, table), {
        get(target, p) {
            if (target[p] === undefined) return table[p];
            else return target[p];
        }
    });
}

export class TableCell implements TableOpts {
    _lines:Line[] = [];
    columns:Column[] = [];
    cellsPadding?:Specs
    cellsBorders?:BorderSpecs

    rowsPadding?:Specs
    rowsBorders?:BorderSpecs

    columnsPadding?:Padding
    columnsBorders?:BorderSpecs

    borderStyle?:BorderStyle
    private _styleEngine:StyleEngine;
    splitLines?:boolean

    private _with:number;
    private _header:Line;

    constructor( opts?:TableOpts, borderStyle?:BorderStyle ) {
        if( !opts ) opts = {};
        Object.assign( this, opts );
        this._styleEngine = new StyleEngine( new Proxy( Object.assign(DEFAULT_BORD_STYLE,this.borderStyle ), {
            get( target, p ){ return borderStyle?.[p] || DEFAULT_BORD_STYLE[ p ]; }
        }));


        this.rowsPadding = spaceOf( this.rowsPadding );
        this.rowsBorders = borderOf( this.rowsBorders );
        this.cellsPadding = spaceOf(  this.cellsPadding );
        this.cellsBorders = borderOf( this.cellsBorders );
        this.columnsPadding = spaceOf(  this.columnsPadding );
        this.columnsBorders = borderOf( this.columnsBorders );

    }

    get columnsLength(){
        return this.columns.map( value => value.len ).reduce( ( p, n )=> p +n );
    }


    get lines():Line[]{
        if( !this.header ) return this._lines;
        else return [ this._header, ...this._lines ];
    }
    push( opts?:LineOpts, ...cells:CellItem[] ){
        let _line = lineOf( opts, cells, this.lines.length, this );
        this.lines.push( _line );
        return _line;
    }

    private static ___border_of( border:BorderOpts, defaultBorder:Border ){
        let _border = borderOf( border );
        return borderOf( {
            top: _border.top||defaultBorder.top,
            bottom: _border.bottom||defaultBorder.bottom,
            left: _border.left||defaultBorder.left,
            right: _border.right||defaultBorder.right,
        })
    }
    cellBorder( celBorder:BorderOpts ):BorderSpecs{
        return TableCell.___border_of( celBorder, this.cellsBorders );
    }

    rowBorder(lineBorder:BorderOpts ):BorderSpecs{
        return TableCell.___border_of( lineBorder, this.rowsBorders );
    }

    isSplit( line:Line ):boolean {
        return  line.split
            || this.splitLines
    }

    get header(){ return this._header; }

    private countColumns( _line:Line ){
        for (let i = this.columns.length; i < _line.cells.length; i++){
            if( !this.columns[i] ) this.columns[i]= {
                len: 0,
                title: String( i ),
                index: i
            }
        }
    }


    private positionOf( line:Line, cell:Cell,  currentColumn = 0 ){
        if( currentColumn === undefined || currentColumn === null ) currentColumn = 0 ;

        let cellJoin = cell.collSpan;
        if( cell.joinMode === "full" ) cellJoin = this.columns.length;
        else if( cell.joinMode === "wrap-content" ){
            cellJoin = (this.columns.length - line.cells.length)+1;
        }
        let columns = this.columns;
        if( line.orientation === "right" ){
            columns = [ ...columns ].reverse();
        }

        let nextColumn = currentColumn + ( cellJoin  );
        // currentColumn = nextColumn - 1;


        if( line.index === 5  ){
            cellJoin = ( this.columns.length - line.cells.length ) +1;
        }

        return {
            cellJoin,
            columns,
            realColumn: columns[ currentColumn ].index,
            currentColumn,
            nextColumn
        }
    }

    hasChange(){
        return true;
    }

    get with(){
        if( !this._with || this.hasChange() ) this.calculate();
        return this._with;
    } get height(){
        return this.lines.length;
    }

    headers( ...cells:CellItem[] ){
        this._header = lineOf( {}, cells, -1, this )
    }

    calculate(){
        let preview:Column[];
        this.columns.splice(0, this.columns.length );
        this.lines.forEach( (line, index, array ) => {
            if( line.cells.length > this.columns.length ) this.countColumns( line );
        });

        this.columns.forEach( value => value.len = 0);
        this.lines.forEach( ( line, index, array ) => {
            let horizontalStyles:CellStyle[] = [];
            if( index === 0 ) horizontalStyles.push( "top" );
            if( index+1 === array.length ) horizontalStyles.push( "bottom" );

            let nextColumn = 0;
            let cells = line.cells;
            if( line.orientation === "right" ) cells = [ ...cells ].reverse();
            cells.forEach( (cell, index) => {
                let styles:CellStyle[] = [ ...horizontalStyles ];
                let cellLength = this.positionOf( line, cell, nextColumn );
                let column = this.columns[ cellLength.realColumn ];
                if( column.index === 0 ) styles.push( "left" );
                if( column.index+ cellLength.cellJoin === this.columns.length ) styles.push( "right" );
                cell.styles = styles;


                let length = this.lengthOfCell( line, cell, cellLength, true )
                if( length > column.len ) column.len = length;
                nextColumn = cellLength.nextColumn;
            });
        });
        this.columns.forEach( value => value.len );
        this._with = this.columns.map( value => value.len ).reduce( ( n, p )=> n+p );
        if( preview ) preview.forEach( value => {
            let find = this.columns.find( (c)=>c.index === value.index)
            if( value.len !== find?.len ){
                throw new Error("Invalid column calculate", );
            }
        })
    }

    show(){
        let { log:_show } = console;
        let showLines:string[] = [];
        let lines = this.lines;
        if( this.header ) lines = [ this.header, ...lines ];

        lines.forEach( (line, index, array ) =>{
            let preview = array[ index-1 ];
            let next = array[ index+1 ];
            showLines.push( ...this.prepareLines( line, preview, next ) );
        });
        _show( showLines.join("\n" ) );
    }

    get bStyle():StyleEngine{
        return this._styleEngine;
    }

    lengthOfCell( line:Line, cell:Cell, { columns, currentColumn, cellJoin }, calculate?:boolean  ):number{
        let len = cell.contentLength
            + line.cellsPadding.lateral
            + this.rowsPadding.lateral
            + this.cellsPadding.lateral
        ;

        let _border = this.cellBorder( cell.border );
        let _rowBorder = this.rowBorder( line.border )
        if( _border.left ) len ++;
        if( _border.right ) len ++;

        if( _rowBorder.right ) len++;
        if ( _rowBorder.left && cell.styles.includes( "left" ) ) len++;

        let collSpams = columns.filter( ( (value, index) => {
            return index >= currentColumn && index  < currentColumn+ cellJoin;
        }));

        if( calculate ){
            // len = this.lengthOfCell( line, cell );

        }  else {
            len = collSpams.map( value => (value.len) ).reduce( ( p, n ) => p+n );
        }


        return len;
    }

    private prepareLines( line:Line, previewLine?:Line, nextLine?:Line ):string[]{
        let split = this.isSplit( line );
        let joinRow = ( ( previewLine && nextLine ) || ( nextLine && !previewLine ) )
            &&!split;

        let hasTopLine = !previewLine || split;


        let lines = [];
        type Target = "table"| "row"| "cell"|"line";
        type LineMap = {
            key?:string,
            order:number,
            orientation: keyof HorizontalBorder,
            target: Target,
            level: number
        };
        const linesMap: LineMap[] = [
            // { orientation:"top", target:"table",  level: 3, order: 3 },
            // { orientation:"top", target: "row",  level: 2, order: 2  },
            // { orientation:"top", target: "cell", level: 1, order: 1 },
            {  orientation:null, target:"line",  level: 0, order: 0 },
            // { orientation:"bottom", target: "cell", level: 1, order: -1 },
            // { orientation:"bottom", target:"row",  level: 2, order: -2 },
            // { orientation:"bottom", target:"table",  level: 2, order: -3 }
        ];

        type Line = LineMap & { pads: string[], line: string, borders: 0 };
        const __lines:Line[] = [];

        let lNes = {
            init(){ this.find( "line" )},
            names:[],
            find( target:Target, orientation?:(keyof HorizontalBorder ) ):Line{
                let _map = linesMap.find( value => value.target === target && (value.orientation||"") === (orientation||"" ));
                let _line = __lines.find( value => value.target === target && (value.orientation||"") === (orientation||"" ) )

                if( !_line && !!_map ) {
                    _line = {
                        ..._map,
                        orientation: _map.orientation,
                        line:"",
                        borders: 0,
                        pads:[],
                        target: _map.target,
                    };
                    __lines.push( _line )
                }
                return  _line;

            }, get list():Line[]{
                return __lines.sort( ( a, b ) => a.order > b.order? -1 : 0 );
            }
        }

        let currentColumn = null;
        let cells = line.cells;
        if( line.orientation === "right" ) cells = [...cells].reverse();
        let realLength = 0;

        lNes.init();
        cells.forEach( (cell, cellIndex) => {

            // let showText = cell.text;
            let cellLength = this.positionOf( line, cell, currentColumn );
            let length = this.lengthOfCell( line, cell, cellLength );
            realLength+= length;

            let borders:{[p in Target]?: BorderSpecs } = {
                cell: this.cellBorder( cell.border ),
                row: this.rowBorder( line.border )
            };

            let letBorder = length;
            if( borders.cell.left ) letBorder --;
            if( borders.cell.right ) letBorder --;
            if( borders.row.right ) letBorder--;
            if ( borders.row.left && cell.styles.includes( "left" ) ) letBorder--;

            (["cell","row"]as const).forEach( ( target) => {
                let _border = borders[ target ];
                if( target === "cell" ) (["bottom", "top" ] as const ).forEach( orientation => {
                    if( !_border[orientation] ) return;
                    lNes.find( target, orientation ).borders++
                })
                if( target === "row" ){
                    if( _border.bottom ) lNes.find( target, "bottom" ).borders++;
                    if( _border.top && hasTopLine ) lNes.find( target, "top" ).borders++
                }
            });

            lNes.list.forEach( value => {
                let _l = lateralBorders( this.bStyle, borders.cell, value.orientation );

                if( value.target === "line" ){
                    if ( borders.row.left && cell.styles.includes( "left" ) ) _l.left = this.bStyle.find( "left" ) + _l.left
                    if( borders.row.right ) _l.right = _l.right + this.bStyle.find( "right" );

                    value.pads = [ value.line, cell.format(text => {
                        return alignLine( text, letBorder, { align: cell.align, char: cell.char })
                    })];

                } else /*if ( value.target === "row" )*/{
                    // let use = letBorder;
                    // if( join && value.target === "row" ) use = use +2
                    // if( value.orientation )
                    value.pads = [ value.line, alignLine( "", letBorder, { align: "left", char: this.bStyle.find( value.orientation ) } ) ];
                }

                if( value.target === "cell" ){
                    if ( borders.row.left && cell.styles.includes( "left" ) ) _l.left = this.bStyle.find( "left" ) + _l.left
                    if( borders.row.right ) _l.right = _l.right + this.bStyle.find( "right" );
                }
                if( value.target === "row" ){
                    // if( join)
                    // if ( borders.row.left && cell.styles.includes( "left" ) ) _l.left = this.bStyle.find( "left" ) + _l.left
                    // if( borders.row.right ) _l.right = _l.right + this.bStyle.find( "right" );
                }

                if( (split||!previewLine||!nextLine) &&  value.target === "row" ){
                    if( (cell.isBetween || !cell.isLast ) && borders.row[ value.orientation ]  )
                        _l.right = this.bStyle.find("join", value.orientation );
                    if( (cell.isBetween || !cell.isFirst) && borders.row[ value.orientation ] ) {
                        _l.left =  this.bStyle.find( "horizontal", "join" )
                    }

                }
                if( value.target == "row"  && joinRow && value.orientation === "bottom" ) {
                    if ( cell.isBetween || !cell.isLast ) _l.right = this.bStyle.find("join" );
                    if( ( cell.isBetween || !cell.isFirst ) && borders.row[ value.orientation ] )
                        _l.left = this.bStyle.find( "horizontal", "join" );

                    if( cell.isFirst && value.orientation === "bottom" )
                        _l.left = DEFAULT_BORD_STYLE["left-join" ];
                    if( cell.isLast && value.orientation === "bottom" )
                        _l.right = DEFAULT_BORD_STYLE["right-join" ];
                }

                if( borders.cell.hasVertical && borders.row.hasAny && value.target === "row" ){
                    _l.right = this.bStyle.find( "horizontal", "join" ) + _l.right;
                    if( cell.isFirst ) _l.left = _l.left + this.bStyle.find( "horizontal", "join" );
                }
                value.pads [ 1 ] = _l.left + value.pads [ 1 ] + _l.right;

            });

            lNes.list.forEach( value => {
                let pads = value.pads;
                if( line.orientation === "right" ) pads.reverse();
                value.line = pads.join("");
            })

            currentColumn = cellLength.nextColumn;
        });

        lNes.list.forEach( value => {
            let opts = ( value.target === "line" )? { align: line.orientation, char: line.char, realLength } :{ char: " ", align: line.orientation}
            value.line = alignLine( value.line, this.with, opts )
        });

        lNes.list.forEach( value => {
            if( value.target === "line" || value.borders  > 0 ) lines.push( value.line );
        })

        return lines;
    }
}

type S = keyof NumberConstructor;

let _s:S[] = [];
_s = [ "isNaN", "isFinite", "isSafeInteger", "isInteger", "parseInt" ];

function testNumber( ){

}

