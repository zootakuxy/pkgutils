import {Table, TableCell} from "./table";
import {borderOf, BorderOpts, BorderSpecs, Cell, CellItem, spaceOf, Specs} from "./cell";

export type LineOpts= {
    border?:BorderOpts,
    orientation?:"left"|"right"|"center"|"justify",
    char?:string,
    split?:boolean
}

export class Line implements LineOpts {
    border?:BorderSpecs
    orientation?:"left"|"right"|"center"|"justify"
    char?:string
    cells:Cell[]
    cellsPadding:Specs
    cellsBorders:BorderSpecs
    index:number
    split:boolean

    constructor( opts?:LineOpts ) {
        if( !opts ) opts = {};
        Object.assign( this, opts );
        this.cellsPadding = spaceOf( this.cellsPadding );
        this.cellsBorders = borderOf( this.cellsBorders );
    }
}

export function lineOf( _opts:LineOpts, cells:CellItem[], index:number, table:TableCell  ):Line{
    let line:Line;
    if( _opts instanceof Line ) return line = _opts;
    else if( !_opts ) _opts = {};
    if( !line )line = new Line( _opts );

    Object.assign( line, {
        get index(){ return index + (  table.header? 1: 0 ) },
        get bodyIndex(){ return index },
        get lineType(){ return index !== -1? "row": "header" }
    })

    const _cells:Cell[]  = cells.map( (cell, index) => {
        let _next:Cell;
        if( cell instanceof Cell ) _next = cell;
        else _next = new Cell( cell );

        _next.collSpan = _next.collSpan || 1;
        return _next;
    });
    _cells.forEach( _next => {
        if( _next.joinMode === "full" && _cells.length > 1 ) throw new Error( `JoinMode:full require one cell only but line have ${ _cells.length } cells!` );
        let otherNoContent:Cell = _cells.find( (n)=> n.joinMode !== "content" && n !== _next );
        if( _next.joinMode === "wrap-content" &&  otherNoContent )
            throw new Error( `joinMode:wrap-content require other cells to be just be content ${ otherNoContent.joinMode } | ${ otherNoContent.text }` );
    });
    line.cells = _cells;
    return line;
}