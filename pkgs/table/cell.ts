import {AlignMode} from "@pkgutils/justify";

export type CellItem = (Cell|CellOpts|string|number|boolean| (()=>CellItem));

export type CollSpanMode ="content"|"wrap-content"|"full";
export type VerticalBorder = {
    left?:boolean
    right?:boolean
}
export type HorizontalBorder={
    top?:boolean
    bottom?:boolean
}

export type Border = VerticalBorder & HorizontalBorder;

export type SpaceOpts =  { lateral?:number, vertical?:number };

export class Specs implements SpaceOpts {
    lateral?:number = 0; vertical?:number = 0;
    constructor( opts?:SpaceOpts ) {
        if( !opts || typeof  opts !== "object" ) opts = {};
        Object.assign( this, opts );
    }
}

export type Padding = Specs | number;
export type Margin = Specs | number;


export type BorderOpts = Border|boolean;

export class BorderSpecs implements Border  {
    private readonly _inConstructor:boolean;
    top?:boolean
    bottom?:boolean
    left?:boolean
    right?:boolean

    constructor( opts?:Border ) {
        this._inConstructor = true;
        Object.assign( this, opts );
        this._inConstructor = false;
        delete Object.assign(this)["_inConstructor"];
    }
    get topLeft(){ return this.top && this.left }
    get topRight(){ return this.top && this.right }
    get bottomLeft(){ return this.bottom && this.left }
    get bottomRight(){ return this.bottom && this.right }

    get specs():(keyof Border )[]{
        let _specs:(keyof Border )[] = [];
        if( this.top ) _specs.push( "top" );
        if( this.left ) _specs.push( "left" );
        if( this.right ) _specs.push( "right" );
        if( this.bottom ) _specs.push( "bottom" );
        return  _specs;
    }

    /**
     * Check if has left or right borders
     */
    get hasVertical(){ return this.left || this.right }
    get hasAny(){ return this.hasVertical || this.hasHorizontal }

    /**
     * Check if has top or bottom borders
     */
    get hasHorizontal(){ return this.top || this.bottom }

    is( ...specs:( keyof BorderSpecs )[]):boolean{
        for (let i = 0; i < specs.length; i++)
            if( !this[specs[ i ] ] ) return false;
        return specs.length > 0;
    }
}

const _allBorders:BorderSpecs[] = [];

function isValidNumber( any:any ){
    let number = Number( any );
    return number
        && typeof number === "number"
        && !Number.isNaN( number )
        && Number.isFinite( number )
}
export function spaceOf( spec:Specs | number ){
    if( spec instanceof Specs ) return spec;
    if( typeof spec === "number" || isValidNumber( spec ) ) spec = {
        lateral: Number( spec ),
        vertical: Number( spec ),
    }
    return new Specs( spec );
}

function find( opts?:BorderOpts ){
    let left = false, right = false, top = false, bottom = false;
    if( (typeof opts === "boolean" && opts ) || ( typeof opts === "object" && opts?.left) ) left = true;
    if( (typeof opts === "boolean" && opts ) || ( typeof opts === "object" && opts?.right) ) right = true;
    if( (typeof opts === "boolean" && opts ) || ( typeof opts === "object" && opts?.top) ) top = true;
    if( (typeof opts === "boolean" && opts ) || ( typeof opts === "object" && opts?.bottom) ) bottom = true;

    let find = _allBorders.find( (next)=>
        next.left === left
            && next.right === right
            && next.top === top
            && next.bottom === bottom
    );
    if( !find ){
        find = new BorderSpecs( { top, left, right, bottom });
        _allBorders.push( find );
    }
    return find;
}


export function borderOf( border:BorderOpts ):BorderSpecs{
    return  find( border );
}



export type CellOpts ={
    border?:BorderOpts
    padding?:Padding,
    text?:string,
    collSpan?:number|CollSpanMode|1|2|3|4|5|6|7|8|9|10,
    align?:AlignMode,
    transform?:CellTransform,
    char?:string
}

export type CellTransform = (text:string)=>string
export const DEFAULT_CELL_TRANSFORM:CellTransform = ( text:string )=> text;

export type CellStyle = "left"|"top"|"right"|"bottom";
export class Cell implements CellOpts{
    _aText?:string
    collSpan:number
    joinMode?:CollSpanMode
    align:AlignMode
    transform:CellTransform
    char?:string
    border:BorderSpecs
    padding: Specs
    styles:CellStyle[];



    constructor( opts:CellItem ) {
        if( !opts ) opts = {};
        if( typeof opts !== "object" ) opts = { text: String( opts ) };
        if( !opts.text ) opts.text = "";

        Object.assign( this, opts );
        if( !this.collSpan ) this.collSpan = 1;
        if( !this.align ) this.align = "left";
        if( !this.transform ) this.transform = DEFAULT_CELL_TRANSFORM;
        if( typeof this.collSpan === "string" ){
            this.joinMode = this.collSpan;
            this.collSpan = 1;
        } else if( !this.joinMode ) this.joinMode = "content";
        this.border = borderOf( this.border );
        this.padding = spaceOf( this.padding );
    }


    get isFirst(): boolean { return this.styles.includes("left") };
    get isLast(): boolean { return this.styles.includes("right") };
    get isBetween(): boolean { return !this.isFirst && !this.isLast };


    set text( text:string){
        this._aText = text;
    }

    get contentLength(){
        return this.text.length
            + ( this.padding.lateral || 0 )
    }
    get text(){  return this._aText }

    get len(){ return (this.text||"").length; }
    format( pre?:{ pre?(text:string):string, pos?( text:string ):string }|((text)=>string ), pos?:( text:string )=>string ):string{
        let text = this.text;
        if( pre && typeof pre === "object" ){
            pos = pre.pos;
            pre = pre.pre;
        }

        if( typeof pre === "function" ) text = pre( text )||text;
        text = (this.transform||DEFAULT_CELL_TRANSFORM)( text )||text;
        if( typeof pos === "function" ) text = pos( text )||text;
        return text;
    }
}
