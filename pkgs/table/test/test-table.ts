import {install} from "source-map-support";
install();

import {createTable} from "../table";

const table = createTable({
    cellsPadding:{ lateral: 1},
    // cellsBorders:true,
    // rowsBorders: true,
    // splitLines: false
});

//
// //                         Name     Surname                 IS03     City          Age      Balance    State
// table.push( {}, { text:"Active Clients", join:"full" } );
// table.push( {}, "Maria",  "Pires Sousa",          "STP",   "Sao Tome",   21,      1576,       "Active" );
// table.push( {}, {text:"Ana Fransisca", join:2},   "PRT",   "Lisbon",     13,      415, { text: "Active", align: "center" });
//
// table.push( {}, { text:"Deleted Clients", align:"right", join:"full" } );
// table.push( {}, "Marcos",  { text:"Unknowns Infos", join:"wrap-content", align: "center"},        "Deleted" );
// table.push( {}, { text:"Disabled Clients", join:"full" } );
// table.push( {}, { text:"Lorem Imp aj s msuha asuahua asja sansbaiua", join:"full", align:"justify" } );
// table.push( { orientation: "right"},                         "Sao Tome",   21,      755,        "Disable" );
// table.push( { orientation: "right"},                 "STP",   "Sao Tome",   21,      755,        "Disable" );


// table( {}, "Ana", "Maria", "23 anos" );
// table( {}, "Ana", "Maria", "23 anos" );
// table( {}, "Ana", "Maria", "23 anos" );
// table( {}, "Ana", "Maria", "23 anos" );
// table( {}, "Ana", "Maria", "23 anos" );
//
table();
//
