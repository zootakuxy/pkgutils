import {OptionType} from "./types";

export type OptionArgs = {
    name: string
    type: OptionType
    value?: any
    alias?: string
    required?:boolean
    multiples?:boolean
    description?:string
    label?:string
}

const DEFAULT_CASTER = ( any )=>{
    return JSON.stringify( any );
}

export class Option implements OptionArgs{
    name: string
    type: OptionType
    value?: any
    alias?: string
    required?:boolean
    multiples?:boolean
    description?:string
    label?:string
    abouts: (any)[] = [];
    valueCaster: ( value:any )=>string

    get stringValue(){
        return (this.valueCaster||DEFAULT_CASTER)( this.value )
    }

    get typeName(){
        return this.type === Number? "NUMBER"
            : this.type === Boolean? "BOOLEAN"
                : this.type === String? "STRING"
                    : "OTHERS"
    }
}
