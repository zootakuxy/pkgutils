import {OptionType} from "./types";
import {Command, CommandArgs, CommandCallback} from "./command";
import {Option, OptionArgs} from "./option";

// type Reverse<Tuple, A, B > = Tuple extends [...A[], ...B[] ]? [...Reverse<B>, A] : [];
type S<A, B > = ([(A|B), ...B[]] & [ (A|B), ...A[]])// &( [...A[]] ) &([...B[]]);
let s1:S<number, string> = [ 1,];

export interface Define {
    command( command:Command, ...opts: (Option | CommandCallback|Command ) [] ):Command
    command( name: string|CommandArgs, ...opts: (Option | CommandCallback|Command)[]): Command;
    command( name: string|CommandArgs, ...opts: (Option | CommandCallback|Command)[] ): Command;

    opt( opt:Option|OptionArgs ):Option;
    opt( name:string, type:OptionType, value?: any, alias?: string, required?:boolean, description?:string, label?:string ):Option
}


function isOption( val:Option|Command|CommandCallback| CommandArgs):false|Option{
    if( isCommand( val ) ) return false;
    if( isCallback( val ) ) return false;
    if( val instanceof Option ) return val;

    let _option:Option = Object.assign( { ...val } );
    if( !_option.name ) return false;
    if( !_option.type ) return false;
    if( typeof _option.type === "function" ) return false;
    return Object.assign( new Option(), _option );

}
function isCommand( val:Option|Command|CommandCallback| CommandArgs ):false|Command{
    if( val instanceof Command ) return val;
    if( !val || typeof val !== "object" ) return false;

    let _command:Command = Object.assign( { ...val } );
    let _option:Option = Object.assign( { ...val } );


    if( _option.type !== undefined ) return false;
    if( _option.required !== undefined ) return false;
    if( _option.multiples !== undefined ) return false;
    if( _option.alias !== undefined ) return false;
    if( _option.value !== undefined ) return false;

    if( _command.commands === undefined ) return false;
    if( !Array.isArray(_command.commands ) ) return false;
    if( _command.options !== undefined && typeof _command.options !== "object" ) return false;
    if( _command.child !== undefined && !Array.isArray(_command.child ) ) return false;
    if( _command.parent !== undefined && !isCommand( _command.parent ) ) return false;
    return Object.assign( new Command(), _command );
}
function isCallback( val: Option|Command|CommandCallback|string| CommandArgs):false|CommandCallback{
    if( typeof val !== "function") return false;
    return val;
}

export const sets = new class implements Define {
    command(command: Command, ...opts: (Option | CommandCallback | Command)[]): Command;
    command(name: string | CommandArgs, ...opts: (Option | CommandCallback | Command)[]): Command;
    command(name: string | CommandArgs, ...opts: (Option | CommandCallback | Command)[]): Command;

    command( command: Command | string | CommandArgs, ...opts: (Option | CommandCallback | Command)[] ): Command {
        if( !command ) return null;
        if( !["function", "object", "string" ].includes( typeof command ) ) return null;
        let _command:Command;

        if( command instanceof Command ){
            _command = command;
        } else {
            _command = new Command();
            if( typeof command === "string"){
                _command.name = command;
            } else if( isCommand( command ) ){
                Object.assign( _command, command );
            } else if( typeof command === "object" ){
                _command.label = command.label;
                _command.name = command.name;
                _command.description = command.description;
            }
        }

        opts.forEach( value => {
            let _child = isCommand( value );
            let _opt = isOption( value );
            let _cal = isCallback( value );

            if( _child ){
                _child.parent = _command;
                _command.child.push( _child );
                if( _child === _command ) _command.commands.push( () => {} );
            }

            if( _opt ){
                _command.options[ _opt.name ] = _opt;
            }
            if( _cal ) _command.commands.push( _cal );
        });
        _command.normalize();
        return _command;
    }

    opt(opt: Option | OptionArgs): Option;
    opt(name: string, type: OptionType, value?: any, alias?: string, required?: boolean, description?: string, label?: string): Option;
    opt(opt: Option | OptionArgs | string, type?: OptionType, value?: any, alias?: string, required?: boolean, description?: string, label?: string): Option {
        if( !opt ) return null;
        if( opt instanceof Option ) return opt;
        const opts = new Option();
        if( typeof opt === "object" ){
            Object.assign( opts, opt );
            return  opts;
        }
        opts.name = opt;
        opts.type = type;
        opts.value = value;
        opts.alias = alias;
        opts.required = required;
        opts.description = description;
        opts.label = label;
        return opts;
    }

}