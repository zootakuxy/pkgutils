export type OptionType = NumberConstructor|BooleanConstructor|StringConstructor|TypeTransform<any>;

export interface TypeTransform<T> {
    transform( str:string ):T
}

export class LineType<T> implements TypeTransform<T>{
    transform(str: string): T {
        let s:any =str;
        return s;
    }
}
