import {Stats} from "fs-extra";
import fs from "fs";
import {LineType} from "./index";

export class TypeFile {
    url: URL
    state: Stats
    exists: boolean
    constructor( string:string ) {
        this.url = new URL( string );
        this.exists = fs.existsSync( string );
        if( this.exists ){
            this.state = fs.statSync( string );
        }
    }

    get href(){
        return this.url.href;
    }
}
export const LINE_TYPE_FILE = new class LineTypeFile extends LineType<TypeFile>{
    transform(str: string): TypeFile {
        return new TypeFile( str );
    }
}