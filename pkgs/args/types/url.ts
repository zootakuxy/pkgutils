import {LineType} from "./index";

export const LINE_TYPE_URL = new class LineTypeUrl extends LineType<URL> {
    transform(str: string): URL {
        return new URL( str );
    }
}
