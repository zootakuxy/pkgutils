import {ArgumentsLine} from "../arguments";
import {Option} from "../option";
import {Capture, Command} from "../command";


export type TextUsage =  { text:string, charsCount:number, originalText?:string };


export interface UsageTemplate {
    descriptionLength:number
    usage( argLine:ArgumentsLine )
    complete(completeLength:number ):{  sep: string,  len:number  }
    important( opt:Option):TextUsage[]
    optionUsage(opt:Option, length?:number, pendentLength?:number): TextUsage
    optionAbout(opt:Option ):TextUsage[]
    defaultInfo( aboutMessage:string|TextUsage[], maxContentLine:number );

    commandUsage(cap:Capture ):TextUsage;
    commandAbout(command:Command):TextUsage[]
}