import { ArgumentsLine } from "../arguments";
import { Command } from "../command";
import { createTable, Table} from "../../table/table";
import { Option } from "../option";
import { CellItem } from "../../table/cell";
import { LineOpts } from "../../table/line";

export type ExtrasLine = {
    when:"before"|"after",
    target: "commandsList"|"command"|"option"|"usage",
    line: (CellItem[]),
    stringType?: "line"|"text"
    opts?: LineOpts
};

export type Targets="commandsList" | "command" | "option" | "usage";

export type Capture = {
    opts?:LineOpts,
    cells:CellItem[]
};
export abstract class BasicUsage {

    usage:string
    argLine:ArgumentsLine;

    constructor( argLine: ArgumentsLine ) {
        this.argLine = argLine;
    }

    abstract onUsage( usage:Table );

    abstract onCommand( usage:Table, command:Command );

    abstract onOptions( usage:Table, command:Command, option:Option );

    abstract extrasLine(table:Table ):ExtrasLine[]

    captureAll():Command[]{
        let captures:Command[] = [];
        this.argLine.commands.forEach( command => {
            captures.push( ...command.capture([ ] ));
        })
        return captures;
    }

    display(){
        let table = this.createTable();
        let extrasLine = this.extrasLine( table );
        const appendExtras = ( target:Targets, callback:()=>void ) => {
            extrasLine.filter( value => value.when === "before" && value.target === target )
                .forEach( value => table.push( value.opts, ...value.line ) );
            callback();
            extrasLine.filter( value => value.when === "after" && value.target === target )
                .forEach( value => table.push( value.opts, ...value.line ) );
        }

        if( this.usage ) appendExtras( "usage", () => this.onUsage( table ) );
        appendExtras( "command", () => {
            this.captureAll().forEach( capture => {
                this.onCommand( table, capture );
                Object.keys( capture.allOptions ).forEach( key => {
                    let option = capture.allOptions[ key];
                    this.onOptions( table, capture, option );
                })
            });
        });

        table();
    }

    createTable():Table {
        return  createTable( {
            cellsPadding: 1
        });
    }
}