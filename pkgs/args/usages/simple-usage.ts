import { Table} from "../../table/table";

const wrapText = require( "wrap-text" );

import { BasicUsage,  ExtrasLine} from "./basic-usage.js";
import { Command} from "../command";
import { Option} from "../option";
import chalk from "chalk";
import { CellItem, CellOpts} from "../../table/cell";

export class SimpleUsage extends BasicUsage {

    contentLength = 50;

    private extrasLineList:ExtrasLine[] = [];

    onUsage(table: Table) {
        table({}, `${this.usage} [...commands] [...options] [...args]`);
    }

    onCommand(table: Table, command: Command) {
        table({}, this.space(), { text: command.path.join(" "), collSpan: "wrap-content" } )
    }

    onOptions(table: Table, command: Command, option: Option) {
        let alias = option.alias ? `-${option.alias},` : "";
        let abouts = this.optionAbout( option );
        let first:CellItem = abouts.shift();
        if( !first ) first = {
            text: "No description found"
        }
        table.push({}, this.space(), this.space(), alias,  `--${option.name}`,
            ...this.important(option), { text:"", collSpan:"wrap-content", align: "right" }, first
        );
        abouts.forEach( extrasAbouts => {
            extrasAbouts.align = "left";
            table({ orientation: "right" },  extrasAbouts );
        });
    }

    pushExtrasLine( ...x:ExtrasLine[] ){ this.extrasLineList.push( ... x ); }

    optionAbout(opt: Option): CellOpts[] {
        let list:CellOpts[] = [];
        const push = ( text:string, transform?:( any:any )=>string ) =>{
            if( !transform ) transform = any => any;
            let lines:string[] = wrapText( text||"", this.contentLength ).split( "\n" );
            lines.forEach( line => {
                list.push( {
                    text: line ,
                    transform: transform
                })
            });
        }
        if( opt.label ) push( opt.label, chalk.underline )
        if( opt.description ) push(opt.description );
        if( opt.stringValue ) push( `Default ${ opt.stringValue}` );

        opt.abouts.forEach( value => {
            if( typeof value === "string" ) push( value );
            else list.push( value )
        });
        return list;
    }

    important( opt:Option):CellItem[]{
        let importantList:CellItem[] = [];
        importantList.push( {
            text: opt.typeName,
            transform: chalk.yellow.underline
        });

        if( opt.required ){
            importantList.push({
                text: ( "REQUIRED" ),
                transform: chalk.underline ,
            });
        }

        if( opt.multiples ){
            importantList.push( {
                text: ( "MULTIPLES" ),
                transform: chalk.greenBright.underline
            });
        }
        return importantList;
    }

    space( repeat?: number ): CellItem {
        if( !repeat ) repeat = 1;
        return { text: "".padEnd( 2 * repeat ) };
    }

    separator( repeat?: number): CellItem{
        return this.space( 3 * ( repeat||1) );
    }


    extrasLine(table:Table ): import("./basic-usage").ExtrasLine[] {
        let extras:ExtrasLine[] = [];
        extras.push( { when: "before", target: "command", line: [{text: "Commands:", collSpan: "full" } ] });
        extras.push( { when: "before", target: "usage", line: [ { text:"Usage:", collSpan: "full"} ] });

        this.extrasLineList.forEach( value => {
            if( !value.stringType ) value.stringType = "line";

            value.line.forEach( ( cell) => {
                cell = cell || "";
                if( typeof cell !== "object" && value.stringType === "text" ){
                    String(wrapText( cell, table.columnsLength )).split( "\n" ).forEach( text => {
                        extras.push( {
                            line: [ { text: text, align: "justify", collSpan: "full" } ],
                            stringType: value.stringType,
                            target: value.target,
                            when: value.when,
                            opts: value.opts
                        });
                    });
                } else if( typeof cell !== "object" ){
                    extras.push({
                        line: [ { text: String( cell ), align: "justify", collSpan: "full" } ],
                        stringType: value.stringType,
                        target: value.target,
                        when: value.when,
                        opts: value.opts
                    })
                } else {
                    cell = Object.assign( cell, {
                        align: "justify", join: "full"
                    })
                    extras.push( {
                        line: [ cell ],
                        stringType: value.stringType,
                        target: value.target,
                        when: value.when,
                        opts: value.opts
                    });
                }
            })

        });
        return extras;
    }
}