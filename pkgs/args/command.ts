

import {TextUsage} from "./usages";
import {sets} from "./sets";
import {Option} from "./option";

export type InheritMode = "keep-inherit"|"override-inherit"|"no-inherit";
export type CommandCallbackArgs = {
    fullPath:string[ ],
    path:string[ ]
};

export type CommandCallback = ( args:CommandCallbackArgs )=>Break|void;

export type CommandArgs = {
    name: string,
    description?: string,
    label?: string
}

export const DEFAULT_COMMAND_NAME = "default";
export const ANY_COMMAND_NAME = "*";


class Break{
    get break(){
        return BREAK
    }
}
export const BREAK = new Break();


export class Command implements CommandArgs {
    id:string
    name: string
    description?: string
    label?: string
    private _options?: {[p:string]:Option} = {}
    parent?:Command
    abouts:string|(string|TextUsage)[];
    inheritMode:InheritMode;

    constructor( name?:string, description?:string, label?:string, inheritMode?:InheritMode ) {
        let min = 1000000000, max = 9999999999;
        let use = (max - min );

        this.id = `${Math.trunc( Math.random() * use + min )}`
        this.name = name|| DEFAULT_COMMAND_NAME;
        this.description = description;
        this.label = label;
        this.inheritMode = inheritMode || (
            [ DEFAULT_COMMAND_NAME, ANY_COMMAND_NAME ].includes( this.name )? "override-inherit"
                : "no-inherit"
        );
    }

    get path():string[]{
        let path:string[] = [ this.name ];
        if( this.parent ) path.unshift( ... this.parent.path );
        return path;
    } get options (){
        return this._options;
    }

    normalize(){
        if( this.child.length && this.commands.length ){
            const deletes:CommandCallback[] = this.commands.splice(0, this.commands.length );
            this.child.push( sets.command( DEFAULT_COMMAND_NAME , deletes.shift(), ...deletes ));
        }
    }

    get allOptions():{[p:string]:Option}{
        let opts:{[p:string]:Option} = {};
        let inheritOpts:{[p:string]:Option} = {};
        if( this.parent && (this.inheritMode||"override-inherit") !== "no-inherit" ) inheritOpts = this.parent.allOptions;
        opts = {...inheritOpts};
        Object.keys( this._options ).forEach( key => {
            let option = this._options[ key ];
            if( !Object.keys( inheritOpts ).includes( key ) ) opts[ key ] = option;
            else if( this.inheritMode === "override-inherit" ) opts[ key ] = option;
        });
        return opts;
    }

    capture( order:string[] ):Command[]{
        if( order.includes( this.id ) ) return [];
        order.push( this.id );

        this.normalize()
        let _list:Command[] = [];

        if( this.child.length ){
            this.child.forEach( value => {
                let _cat = [...order];
                _list.push( ... value.capture( _cat ) );
            })
        } else if( this.commands.length ) {
            _list.push( this );
        }
        return _list;
    }

    launch( opts:CommandCallbackArgs ){
        for (let i = 0; i <this.commands.length; i++) {
            let value = this.commands[ i ];
            if( typeof value !== "function") continue;
            let _break = value( opts );
            if( !_break ) continue;

            if( _break instanceof Break && _break === BREAK && _break.break === _break ) break;
        }
    }

    execute( opts:CommandCallbackArgs ):boolean{
        let _opts:CommandCallbackArgs = JSON.parse( JSON.stringify( opts ) );
        let nextName = _opts.path.shift();
        this.normalize();

        if( !this.child.length && this.commands.length ){
            this.launch( opts );
            return true;
        } else if( !this.child.length ) return false;

        let _next = this.child.find( value => value.name === nextName );
        let _default = this.child.find( value => value.name === DEFAULT_COMMAND_NAME );
        let _any = this.child.find( value => value.name === ANY_COMMAND_NAME );

        if( _next ) return _next.execute( _opts );
        else if( _any ) return _any.execute( opts );
        else if( !_next && !nextName && _default ) return _default.execute( opts );
        return false;
    }
    child:Command[] = []
    commands:CommandCallback[] = []
}
