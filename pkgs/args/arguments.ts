import map from 'source-map-support';
map.install();

import {Command, CommandCallback, ANY_COMMAND_NAME, DEFAULT_COMMAND_NAME} from "./command";

require("source-map-support").install();

import {sets} from "./sets.js";
import {Option} from "./option";
import {SimpleUsage} from "./usages/simple-usage";


export class ArgumentsLine {
    private readonly _commands:Command[];

    constructor( usage?:string, about?:string ) {
        this._commands = [];
    }

    define( ...commands:Command[] ){
        commands.forEach( value => {
            if(!( value instanceof Command )) return;
            if( this._commands.find( value1 => value1.name === value.name ) ) return;
            this._commands.push( value );
        });
    }

    get commands ():Command[]{
        return this._commands;
    }

    private _special(name, callback:CommandCallback, ...opts:(Option|CommandCallback)[] ){
        let _command = this._commands.find( value => value.name === name );
        if( !_command ) {
            this._commands.unshift( _command );
        } else sets.command( name, callback, ...opts );
    }

    any( callback:CommandCallback, ...opts:(Option|CommandCallback)[] ){
        return this._special( ANY_COMMAND_NAME, callback, ...opts );
    }

    default( callback:CommandCallback, ...opts:(Option|CommandCallback)[] ){
        return this._special( DEFAULT_COMMAND_NAME, callback, ...opts );
    }
}
let aLine:ArgumentsLine = new ArgumentsLine( "maguita", "Lorem vitae quasi quaerat et consectetur quis ea! Eos unde repellendus soluta eaque accusamus deserunt maiores? Facere nihil architecto facilis fuga quidem? Nostrum animi consectetur quis atque architecto totam molestias.");
aLine.define(
    sets.command( { name:"maguita", description: "This Is a Test Command"},
        sets.opt( "port", Number, 5432, "p" ),
        sets.opt( { name: "host", type: Number, required: true, label: "Host Name"} ),
        sets.opt( { name: "dbkwdjkwhiwhdiduhduihw", type: Number, required: true, label: "Host Name", description: "Usede to connect skfn fenjfen feijbfe fifeifbe feyubfeefbe  fe", alias: "d"} ),
        ()=>{},
        sets.command( "version", args => {},
            sets.command( "status", args => {} ),
            sets.command( "update", args => {} ),
        )
    )
)
new SimpleUsage( aLine ).display();

