export type Comment = {
    begin?:number,
    end?:number,
    codeStart?:number,
    content?:string,
    info?:{ type?:"multiline"|"singleline"},
    code?:string
}
export type CommentMap<T extends Comment > = { [p:string]:T };