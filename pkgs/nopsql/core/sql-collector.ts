import { scanDir} from "@pkgutils/fdir";
import { extension, filter, regExp, ScanFilter} from "@pkgutils/fdir/scan";
import { Dependency, DependencyResolver} from "@pkgutils/deps";
import { sqlExtractor, SQLPath} from "./sql-extract";
import * as path from "path";
import * as fs from "fs";
import {createUrlFile} from "@pkgutils/fdir/path";
export type SQLCollectorOpts = {
    baseName?:string,
    distName?:string,
    operation?:"init" | "test" |"compile"|"pre-publish"|"release"|"upgrade"|"update"|"fetch"
};

export const DEFAULT_EXCLUSION = /(!.*.sql)/;
export const PACKAGE_NAME = "nopsql.json";
export const DEFAULT_DIST_NAME = "./database/dist";
export const DEFAULT_BASE_NAME = "./database/src";
export const DEFAULT_DIST_FILE = "nopsql-dist.json";
export const DEFAULT_EXTENSION = ".sql$";


const optsVal = ( opts?:SQLCollectorOpts, replacer?:SQLCollectorOpts )=>{
    if( !opts ) opts = {};
    if( !replacer ) replacer = {};
    if( opts.baseName === undefined ) opts.baseName = replacer.baseName;
    if( !opts.baseName ) opts.baseName = replacer.baseName || process.cwd();
    return opts;
}

export type Status = {
    distName?:string,
    baseName:string
    defaultExclusion?:boolean,
    exclusion:string[],
    recursive?:boolean|true,
    extension?:(string|".sql")[],
    createSrc?: boolean
}

export type DistStatus = Status & {
    deps:string[],
    compiledDeps:string[],
    version?:number,
    lastDate?:string,
    lastCompile?:{
        version: number,
        status?: "success"|"failed"|"forced",
        path?: string,
        failed?:string[]
    },
}

type VersionInfo = {
    verDir:string,
    src:string[],
    compile?:{
        status?: "success"|"failed"|"forced",
        path?: string,
        failed?:string[]
    }
};

export function init( opts:SQLCollectorOpts ){

    let nonpsqlJson = path.join( process.cwd(), PACKAGE_NAME );
    let  status:Status;
    if( fs.existsSync( nonpsqlJson ) ){
        status = JSON.parse( fs.readFileSync( nonpsqlJson, "utf-8" ) );
    } else status = { distName:undefined,  exclusion:[], baseName: opts.baseName|| "./" }

    if(!status.baseName ) status.baseName = opts.baseName || "./";
    let  distStatus:DistStatus = {  deps:[], exclusion:[], compiledDeps:[], baseName: status.baseName };

    if( status.distName === undefined ) status.distName = DEFAULT_DIST_NAME;

    const distDir = path.join( path.dirname( nonpsqlJson ), status.distName );
    const baseDir = path.join( path.dirname( nonpsqlJson ), status.baseName );
    const distPackage = path.join( distDir, DEFAULT_DIST_FILE );
    if( fs.existsSync( distPackage ) ) distStatus = JSON.parse( fs.readFileSync( distPackage, "utf-8" ) );
    if( !distStatus.deps ) distStatus.deps = [];



    if( !distStatus.version  ) distStatus.version = 0;
    if( !status.exclusion ) status.exclusion = [];

    if( status.recursive === undefined ) status.recursive = true;
    if( status.defaultExclusion === undefined ) status.defaultExclusion = true;
    if( !status.extension ) status.extension = [ DEFAULT_EXTENSION ];
    if( status.createSrc === undefined ) status.createSrc = true;

    Object.assign( distStatus, status );
    return { status, nonpsqlJson, distStatus, distPackage, distDir, baseDir };
}
function fileStatus (opts:SQLCollectorOpts ):( {status:Status, version:VersionInfo, package:string,
    distDir: string,
    baseDir: string,
    distPackage:string
    verPackage: string,
    verSrc:string,
    verPath:string,
    distStatus: DistStatus
} ) {

    const {status, nonpsqlJson, distStatus, distDir, distPackage, baseDir } = init( opts );
    distStatus.lastDate = new Date().toISOString();
    distStatus.version++;

    const version:VersionInfo = {
        verDir: path.join( distDir, "versions", `v${distStatus.version}` ),
        src: [],
    }

    return { status, version, package:nonpsqlJson, distDir,
        baseDir,
        distPackage,
        distStatus,
        verPackage:  path.join( distDir, "versions", `v${distStatus.version}`, `nonpsql-v${ distStatus.version }.json`),
        verPath: path.join( distDir, "versions", `v${distStatus.version }`, `upgrade-v${ distStatus.version }.sql` ),
        verSrc: path.join( distDir, "versions", `v${distStatus.version}`, "src" )
    };
}

export class SQLCollector {
    private readonly base:string;

    constructor( opts?:SQLCollectorOpts ) {
        opts = optsVal( opts );
        this.base = opts.baseName;
    }

    collect( opts?:SQLCollectorOpts ){
        opts = optsVal( opts, { baseName: this.base  });
        const sts = fileStatus( opts );

        fs.mkdirSync( sts.baseDir, { recursive: true });
        fs.mkdirSync( sts.distDir, { recursive: true });
        if( sts.status.createSrc ) fs.mkdirSync( sts.verSrc, { recursive: true });

        const versionPath = fs.createWriteStream( sts.verPath, "utf-8" );


        let filters:ScanFilter[] = [];
        const distPathPart = sts.distDir.split( path.sep );
        sts.status.extension.forEach( extensionName => {
            let next = extension( extensionName );
            //Excludes from dist dirs
            next.not( filter( path1 => {
                let pathParts = path1.path.split( path.sep );
                return !distPathPart.find( ( distPart, index) => {
                    return pathParts[ index ] !== distPart;
                });
            }))

            //Defaults Exclusion
            if( sts.status.defaultExclusion ) next.not( regExp( DEFAULT_EXCLUSION ) );

            //Other exclusions
            sts.status.exclusion.forEach( regex => {
                next.not( regExp( RegExp( regex ))  )
            });
            filters.push( next );
        });

        const paths = scanDir( sts.baseDir, { recursive: true }, ...filters, p => {
            console.log( "FOUND ", createUrlFile( p.path ).href );
        });

        const resolver = new DependencyResolver();
        const dependencies:Dependency[] = [];

        paths.forEach( next => {
            sts.version.src.push( next.relative );
            if( sts.status.createSrc ) {
                let srcDir = path.join( sts.verSrc, path.dirname( next.relative ) );
                fs.mkdirSync( srcDir, { recursive: true } );
                fs.copyFileSync( next.path, path.join( srcDir, path.basename( next.relative ) ) );
            }
            const extractors = sqlExtractor( next.relative, {
                base: sts.baseDir
            });

            extractors.forEach( value => {
                if( dependencies.find( deps => deps.id === value.id ) ) throw new Error( `Already dependency ID: ${ value.id } is declared` );
                dependencies.push( resolver.create( value.id, value ) );
            });
        });

        dependencies.forEach( currentDeps => {
            let sqlPath:SQLPath = currentDeps.value();
            sqlPath.before.forEach( nextBeforeId => {
                let nextDeps = dependencies.find( value1 => value1.id === nextBeforeId );
                currentDeps.before( nextDeps );
            })
            sqlPath.after.forEach( nextBeforeId => {
                let nextDeps = dependencies.find( value1 => value1.id === nextBeforeId );
                currentDeps.after( nextDeps );
            });
        });

        resolver.ignores();
        resolver.order();
        var input = new Buffer('lorem ipsum dolor sit amet');

        resolver.list.forEach( value => {
            let sqlPath = value.value<SQLPath>();
            if( sqlPath.isOne && sts.distStatus.deps.includes( value.id ) ) return;

            versionPath.write( sqlPath.raw )
            versionPath.write( "\n" );
            versionPath.write( sqlPath.sql )
            versionPath.write( "\n" );
            if( !sts.distStatus.deps.includes( value.id ) ) sts.distStatus.deps.push( value.id );
        });

        versionPath.on( "close", () => {
            fs.writeFileSync( sts.package, JSON.stringify( sts.status, null, 2 ) )
            fs.writeFileSync( sts.verPackage, JSON.stringify( sts.version, null, 2 ) );
            fs.writeFileSync( sts.distPackage, JSON.stringify( sts.distStatus, null, 2 ) );
        });

        versionPath.close();

    }
}