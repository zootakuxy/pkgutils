
import {Comment, CommentMap} from "./types";
import * as path from "path";
import * as fs from "fs";
import {createUrlFile} from "@pkgutils/fdir/path";
const mec = require('multilang-extract-comments');


const SQL_OPTS = {
    pattern: {
        name: 'SQL',
        nameMatchers: [ '.sql' ],
        // singleLineComment: [{ start: '/**!' }],
        multiLineComment: [{ start: '/**!', middle: '', end: '!*/'}]
    }
};

export interface SQLPath {
    context?:Comment,
    isDefault?:boolean
    lineIndex?:number
    lines?:string[]
    id?:string,
    isOne?:boolean,
    after?:string[],
    before?:string[],
    sql?:string,
    raw:string
}

function extract( str ){
    const regxFuncName = /([^(]+)/;
    const regxArgs = /\(\s*([^)]+?)\s*\)/;
    let result = regxFuncName.exec( str );
    if( !result ) return null;
    const funcName = result[1];
    result = regxArgs.exec( str );
    let args = [];
    if( result && result[1] ){
        args = result[1].split(/\s*[(,|\s)]\s*/)
    }

    return {
        name: funcName,
        args
    }
}

const functions = [ "@+id", "@id", "@before", "@after", "@one" ];

export type Opts = { base?:string }
export function sqlExtractor( entry:string, opts?:Opts, list?:SQLPath[]):SQLPath[]{

    if( !opts ) opts = {};
    if( !opts.base ) opts.base = process.cwd();
    let fileName = path.join( opts.base, entry );
    const sql = fs.readFileSync( fileName, "utf-8" );

    let comments:CommentMap<Comment> = mec( sql, SQL_OPTS );

    if( !list ) list = [];

    Object.keys( comments ).forEach( key => {
        let comment = comments[ key ];
        let uPath:SQLPath = { after:[], before:[], isOne: false, sql: "", isDefault: false, get raw(){
            let _raw:string = "";
            this.lines.forEach( (value, index) =>{
                if( index > 0 ) _raw+= "\n";
                _raw+= `  ${ value }`
            });
            if( _raw.length ) _raw = `/**!\n${_raw}\n*/`
            else  _raw = `/**!\n*/`
            return _raw;
        }};
        uPath.lineIndex = Number( key );
        uPath.context = comment;

        uPath.lines = comment.content.split("\n")
            .map( value => value.trim() )
            .filter( value => value.length )
            .filter( value => {
                let reg = extract( value );
                return !!reg && functions.includes( reg.name );
            });

        const valueOf= ( str, ...func ):any[]=>{
            let reg = extract( str );
            if( !func.includes( reg.name ) ) return null;
            return reg.args;
        }

        uPath.lines.forEach( line => {
            let id = valueOf( line, "@id", "@+id" )?.[0];
            if( id && id === "default" ) throw new Error( "DEFAULT id is reserved" );
            if( id && uPath.id ) throw new Error( `ID Already declarede in ${ entry } line ${ uPath.lineIndex }` );
            if( id && list.find( value => value.id === id ) ) throw new Error( "Duplicated id" );
            if( id ) uPath.id = id;

            let after = valueOf( line,  "@after" );
            if( after ){
                after.forEach( next => {
                    if( !uPath.after.includes( next ) ) uPath.after.push( next );
                });
            }

            let one = valueOf( line, "@one" );
            if( one ) uPath.isOne = true;

            let before = valueOf( line,  "@before" );
            if( before ){
                before.forEach( value => {
                    if( !uPath.before.includes( value ) ) uPath.before.push( value );
                });
            }
        });
        let fName = path.join( opts.base, entry );
        let url = createUrlFile( fName, comment.begin, 1 );
        if ( !uPath.id ) throw new Error( `No ID found in block file: ${url.href }` );

        list.push( uPath );

    });
    let sqLines= sql.split( "\n" );

    if( !list.length || list[0].context.begin > 1 ){
        let _default:SQLPath = {
            raw: "",
            context: {
                begin: 0,
                end: 0,
                codeStart: 1,
                code: sqLines[0]
            },
            id: `default|file:\\\\$src\\${ entry }`,
            isDefault: true,
            after: [],
            before: [],
            lineIndex: 0,
            lines:[],
            isOne: false,
            sql: ""
        };
        list.forEach( value => {
            _default.before.push( value.id )
        });
        list.unshift( _default );
    }

    for ( let i = 0; i < list.length-1; i++ ) {
        let current = list[i];
        let next = list[i+1];
        sqLines.filter( (value, idx) => idx >= current.context.codeStart-1 && idx < next.context.begin-1 )
            .forEach( (value, index) =>{
                if( index > 0 ) current.sql += "\n";
                current.sql+= value;
            });
    }
    const last = list[ list.length -1 ];
    sqLines.filter( (value, idx) => idx >= last.context.codeStart-1 && idx < sqLines.length )
        .forEach( value => last.sql+= value+"\n" );

    return list;
}