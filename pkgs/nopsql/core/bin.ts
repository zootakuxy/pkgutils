// #!/usr/bin/env node
// require( 'source-map-support' ).install();
//
// import {Arguments} from "@pkgutils/args";
// import * as fs from "fs";
// import * as path from "path";
// import {DEFAULT_BASE_NAME, DEFAULT_DIST_NAME, init, PACKAGE_NAME, SQLCollector} from "./sql-collector";
//
// type ArgsOpts = {
//     baseDir: string,
//     distDir:string
// }
//
// const args = new Arguments<ArgsOpts>();
//
// args.define({ name: "baseDir", type: String, value: DEFAULT_BASE_NAME });
// args.define({ name: "distDir", type: String, value: DEFAULT_DIST_NAME });
//
// args.defineCommand( { name: "init", callback:receiver => {
//     let argv = args.values;
//     if( !argv.baseDir ) throw new Error( "Base dir is not declared" );
//     if( fs.existsSync( path.join( process.cwd(), PACKAGE_NAME ) ) ){
//         console.error( ` nompsql.json already init` );
//         return;
//     }
//     let sts = init( { baseName: argv.baseDir, distName: argv.distDir } );
//     fs.mkdirSync( argv.baseDir, {recursive:true} );
//     fs.writeFileSync( sts.nonpsqlJson, JSON.stringify( sts.status, null, 2 ) );
//
// }});
//
// args.defineCommand( { name: "build", callback:receiver => {
//     if( !fs.existsSync( path.join( process.cwd(), PACKAGE_NAME ) ) ){
//         console.error( ` nompsql.json not found init` );
//         return;
//     }
//     const coll = new SQLCollector();
//     coll.collect();
// }});
//
// args.execute();
//
//
// // stable  - corresponde ao estado atualmente em produção
// // candidate - corresponde ao proximo estado para produção
// // developer - corresponde ao banco de dados atualamente em produção
// /**
//  * init
//  * test
//  *  - Copiar estado da base de dados candidate e colocar na base de dados test
//  *  - Executar executar apenas os path que ainda não foram compilados
//  *  - Avaliar o relatori
//  * compile
//  *  - Aplicar os novos paths de atualizção testado com successo e colocar na base de dados candidate
//  *  - Transportar o estado da base de dados candidate e colocar na base de dados de teste
//  *  - Marcar o path de atualizaçao como candidato
//  * pre-publish
//  *  - Disponibilizar os path do test e os path compilados para (fetch e update)
//  *
//  * release
//  *  - Compilar todos os novos path de atualiazção no banco de dados statable
//  *  - Disponibilizar o path candidado para publico (para upgrade)
//  *  - Transportar o estado do stable para release e para test
//  *
//  * fetch
//  *  - Importar todas as atualizações disponivel para o banco de dados de teste (incluindo candidade, release e test )
//  * update
//  *  - Importar totas as atualizacao candidate disponivel para o banco de dados canidadete e test
//  *
//  * upgrade
//  *  - Importar apenas as atualizações release para o banco de dados stable, candidate, test
//  *
//  *
//  *
//  *
//  */


console.log(process.argv );
console.log(process.argv0 );
console.log(process.execArgv );