type Append = ( str:string, char:string )=>string;
const LEFT_APPEND:Append = (str, c) => str.padStart( str.length+1, c );
const RIGHT_APPEND:Append = (str, c) => str.padEnd( str.length+1, c );

type AlignText = { index:number, padding:Append, name:AlignMode };


const align = {
    left( preview:AlignText, length ):AlignText{
        return {
            name: "left",
            index: length-1,
            padding: RIGHT_APPEND
        }
    },
    right( preview:AlignText, length ):AlignText{
        return {
            name: "right",
            index: 0,
            padding: LEFT_APPEND
        }
    }, center( preview:AlignText, length ):AlignText{
        if( !preview || preview.name === "right" ) return this.left( preview, length );
        else return this.right( preview, length );
    },

    justify( preview:AlignText, length ):AlignText{
        let next:number;
        if( !preview || length === 1 ) next = 0;
        else if( preview.index+2 === length ) next = 0;
        else next = preview.index+1;
        return {
            name: "justify",
            index: next,
            padding: RIGHT_APPEND
        }
    },
} as const;

export type AlignMode = keyof typeof align;
// @ts-ignore
export const ALIGNS:AlignMode[] =[ ...Object.keys( align )];

export type JustifyLineOpts = {
    realLength?:number,
    char?:string,
    break?:string,
    join?:string,
    align?:AlignMode
}

function toOpt<T extends JustifyLineOpts>( opts?:JustifyLineOpts| AlignMode ):T{
    let _t:any;
    if( ! opts ) _t = {};
    else if( typeof opts === "string" ) _t = { align: opts };
    else _t = opts;
    return _t;
}

export function alignLine(line:string, length:number, opts?:JustifyLineOpts|AlignMode ){
    opts = toOpt( opts );
    if( !opts.realLength ) opts.realLength = line.length;
    if( !line ) line = "";
    if( !length === undefined || length === null || Number.isNaN( length ) || typeof length !== "number" )
        throw new Error( "Length number required!" );
    if( opts.realLength > length ) throw new Error( `line.length:${ opts.realLength} > length${ length }! $BEGIN_LINE|${line}|$END_LINE` );

    let invalidsStrings:any[] = [ undefined, null, false, true ];
    if( !opts ) opts = {};
    if( invalidsStrings.includes( opts.char )  ) opts.char = " ";
    if( invalidsStrings.includes( opts.break ) ) opts.break = " ";
    if( invalidsStrings.includes( opts.join ) ) opts.join = opts.break;
    if( invalidsStrings.includes( opts.align ) ) opts.align = "left";

    opts.join = opts.join.substr( 0, opts.break.length ).padEnd( opts.break.length, opts.char )
    opts.char = opts.char.charAt(0);
    let missing = length - line.length;
    let parts =  line.split( opts.break );


    let next:AlignText;

    for ( let i = 0; i < missing; i++ ) {
        next = align[ opts.align ]( next, parts.length );
        let part = parts[ next.index ];
        part = next.padding( part, opts.char ); // part.padEnd(part.length+1, opts.char );
        parts[ next.index ] = part;
    }
    line =  parts.join( opts.join );
    return line;
}

export type JustifyMultiplesLinesOpts = JustifyLineOpts & { min?:number, max?:number };

export function justifyMultiplesLines( lines:string[], opts?:JustifyMultiplesLinesOpts|AlignMode ){
    let _opts = toOpt<JustifyMultiplesLinesOpts>( opts );
    if( !_opts.min ) _opts.min = 0;
    let max = _opts.min;

    lines.forEach( nLine => {
        if( nLine.length > max ) max = nLine.length;

        if( _opts.max && _opts?.max < max ) throw new Error( "maximum limit exceeded!" );
    });
    return lines.map( line => alignLine( line, max, opts ));
}

export type JustifyOpts = JustifyMultiplesLinesOpts & {
    breakText?:string,
    joinText?:string
}

export function justify( text:string, opts?:JustifyOpts|AlignMode ){
    opts = toOpt( opts );
    let invalidsStrings:any[] = [ undefined, null, false, true ];
    if( !opts ) opts = {};
    if( invalidsStrings.includes( opts.breakText )  ) opts.breakText = "\n";
    if( invalidsStrings.includes( opts.joinText ) ) opts.joinText = opts.breakText;
    opts.joinText = opts.join.substr( 0, opts.breakText.length ).padEnd( opts.breakText.length, opts.char );

    return justifyMultiplesLines( text.split( opts.breakText ) )
        .join( opts.joinText );
}
